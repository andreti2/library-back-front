import { Controller, Get, Post, Put, Delete, Body, Param, Query, ParseIntPipe, Req, HttpStatus, HttpCode } from '@nestjs/common';
import { BookRequestDto } from 'src/dto/request/book.dto.request';
import { BookUpdateRequestDto } from 'src/dto/request/book.update.dto.request';
import { BookResponsetDto } from 'src/dto/response/book.dto.request';
import { BookService } from 'src/service/book.service';

@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) { }

  @Get()
  @HttpCode(HttpStatus.OK)
  findAllPageable(@Query('page') page: number, @Query('limit') limit: number) {
    console.log(2, page, limit)
    return this.bookService.findAllPageable(page, limit);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: number): Promise<BookResponsetDto> {
    return this.bookService.findOne(+id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() book: BookRequestDto): Promise<BookResponsetDto> {
    console.log("add:", book)
    return this.bookService.create(book);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  update(@Param('id') id: number, @Body() book: BookUpdateRequestDto): Promise<BookResponsetDto> {
    return this.bookService.update(id, book);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id') id: number): Promise<void> {
    return this.bookService.remove(id);
  }
}
