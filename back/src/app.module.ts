import { Module, ValidationPipe } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookEntity } from './entity/book.entity';
import { BookController } from './controller/book.controller';
import { BookService } from './service/book.service';
import { APP_FILTER, APP_PIPE } from '@nestjs/core';
import { GlobalException } from './exception/global.exception';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      synchronize: true,
      logging: true,
      entities: [BookEntity],
      autoLoadEntities: true

    }),
    TypeOrmModule.forFeature([BookEntity]),
  ],
  controllers: [BookController],
  providers: [
    BookService,
    {
      provide: APP_FILTER,
      useClass: GlobalException,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },],
})
export class AppModule { }
