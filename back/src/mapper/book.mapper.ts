import { BookRequestDto } from "src/dto/request/book.dto.request";
import { BookUpdateRequestDto } from "src/dto/request/book.update.dto.request";
import { BookResponsetDto } from "src/dto/response/book.dto.request";
import { BookEntity } from "src/entity/book.entity";

export function requestToEntity(request: BookRequestDto | BookUpdateRequestDto) {
    const entity: BookEntity = {
        title: request.title,
        description: request.description,
        status:request.status
    }
    return entity
}

export function entityToResponseList(entities: BookEntity[]): BookResponsetDto[] {
    const listResponse = entities.map(entity => {
        return entityToResponse(entity)
    })
    return listResponse
}

export function entityToResponse(entity: BookEntity):BookResponsetDto {
    const response: BookResponsetDto = {
        id: entity.id,
        title: entity.title,
        description: entity.description,
        status:entity.status,
        createdAt: entity.createdAt.toString()
    }
    return response
}