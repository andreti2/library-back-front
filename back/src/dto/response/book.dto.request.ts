import { StatusBook } from "src/enum/status.book.enum";

export class BookResponsetDto {
  id: number;
  title: string;
  description: string;
  status:StatusBook;
  createdAt:string;
}
