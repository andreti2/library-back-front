import { IsEnum, IsNotEmpty, IsOptional, IsString, MaxLength, max } from "class-validator";
import { StatusBook } from "src/enum/status.book.enum";

export class BookUpdateRequestDto {
  @IsString()
  @IsOptional()
  @MaxLength(200, {message:"campo 'title' caracteres permitidos como maximo 200"})
  title: string;

  @IsString()
  @IsOptional()
  @MaxLength(500, {message:"campo 'description' caracteres permitidos como maximo 500"})
  description: string;

  @IsEnum(StatusBook, { message: "Valores permitidos ('IN_STOCK','OUT_OF_STOCK')" })
  @IsOptional()
  status: StatusBook;
}
