import { IsEnum, IsNotEmpty, IsString, MaxLength } from "class-validator";
import { StatusBook } from "src/enum/status.book.enum";

export class BookRequestDto {
  id: number;

  @IsString()
  @IsNotEmpty({ message: "Campo 'title' es obligatorio" })
  @MaxLength(200, {message:"campo 'title' caracteres permitidos como maximo 200"})
  title: string;

  @IsString()
  @IsNotEmpty({ message: "Campo 'description' es obligatorio" })
  @MaxLength(500, {message:"campo 'description' caracteres permitidos como maximo 500"})
  description: string;

  @IsEnum(StatusBook, { message: "campo 'status' es obligatorio. Valores permitidos ('IN_STOCK','OUT_OF_STOCK')" })
  status: StatusBook;
}
