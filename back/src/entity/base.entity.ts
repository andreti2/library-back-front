import { CreateDateColumn, PrimaryGeneratedColumn } from "typeorm";

export abstract class BaseEntity {
    @PrimaryGeneratedColumn()
    public id?: number;
  
    @CreateDateColumn()
    public createdAt?: Date;
  }