// src/tasks/task.entity.ts
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { BaseEntity } from './base.entity';
import { StatusBook } from 'src/enum/status.book.enum';

@Entity("book")
export class BookEntity extends BaseEntity {
  @Column()
  title: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: StatusBook,
    default: StatusBook.IN_STOCK,
  })
  status:StatusBook
}
