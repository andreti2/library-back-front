import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookRequestDto } from 'src/dto/request/book.dto.request';
import { BookUpdateRequestDto } from 'src/dto/request/book.update.dto.request';
import { BookResponsetDto } from 'src/dto/response/book.dto.request';
import { BookEntity } from 'src/entity/book.entity';
import { entityToResponse, entityToResponseList, requestToEntity } from 'src/mapper/book.mapper';
import { Repository } from 'typeorm';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,
  ) { }

  async findAllPageable(page: number = 1, limit: number = 10): Promise<BookResponsetDto[]> {
    try{
      const skip = (page - 1) * limit;
      return entityToResponseList(await this.bookRepository.find({ skip, take: limit, order: { createdAt: 'DESC' } }));
    }catch(e){
      throw new HttpException({ message: process.env.ERROR_MSG_FIND + '. ' +e.message }, HttpStatus.PRECONDITION_FAILED)
    }
  }

  async findOne(id: number): Promise<BookResponsetDto> {
    const book = await this.bookRepository.findOneBy({ id })
    if(book==null){
      throw new HttpException({ message: process.env.ERROR_MSG_NOT_FOUND }, HttpStatus.NOT_FOUND)
    }
    return entityToResponse(book)
  }

  async create(request: BookRequestDto): Promise<BookResponsetDto> {
    try{
      return entityToResponse(await this.bookRepository.save(requestToEntity(request)));
    }catch(e){
      throw new HttpException({ message: process.env.ERROR_MSG_CREATE + '. ' +e.message }, HttpStatus.PRECONDITION_FAILED)
    }
    
  }

  async update(id: number, request: BookUpdateRequestDto): Promise<BookResponsetDto> {
    const res = await this.bookRepository.update(id, requestToEntity(request));
    if (res.affected == 0) {
      throw new HttpException({ message: process.env.ERROR_MSG_NOT_FOUND }, HttpStatus.NOT_FOUND)
    }
    return this.findOne(id)
  }

  async remove(id: number): Promise<void> {
    const res = await this.bookRepository.delete(id);
    if (res.affected == 0) {
      throw new HttpException({ message: process.env.ERROR_MSG_NOT_FOUND }, HttpStatus.NOT_FOUND)
    }
  }
}
