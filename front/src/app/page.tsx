"use client";
import Table from './components/TableComponent';
import axios from './utils/axios';
import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';

export interface BookModel {
  title: string
  description: string
  status: string
  id?: number
}

const LIMIT_ELEMENTS_PAGE = 5
const DEAFULT_PAGE = 1

function buildNewItem(): BookModel {
  return { title: '', description: '', status: '', id: -1 }
}

const Home = () => {
  const [items, setItems] = useState([]);
  const [newItem, setNewItem] = useState(buildNewItem());
  const [id, setId] = useState(-1);
  const [currentPage, setCurrentPage] = useState(DEAFULT_PAGE);

  useEffect(() => {
    fetchData(currentPage);
  }, []);

  const fetchData = async (page: number) => {
    try {
      const queryParams = {
        page: String(page),
        limit: String(LIMIT_ELEMENTS_PAGE),
      };
      const queryString = new URLSearchParams(queryParams).toString();
      const response = await axios.get(`/book?${queryString}`);
      setCurrentPage(page)
      setItems(response.data);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  const handleAddOrUpdateItem = async () => {
    try {

      const addOrUpdate = { title: newItem.title, description: newItem.description, status: newItem.status }
      console.log(addOrUpdate)
      if (id > -1) {
        await axios.put('/book/' + id, addOrUpdate);
      } else {
        await axios.post('/book', addOrUpdate);
      }
      fetchData(currentPage)
      setNewItem(buildNewItem());
      setId(-1)
      alert('Registro guardado correctamente !!')
    } catch (error) {
      console.error('Error al guardar elemento:', error);
      alert('Error al guardar el elemento: ' + error)
    }
  };

  const handleDeleteItem = async (id: number) => {
    try {
      await axios.delete(`/book/${id}`);
      fetchData(currentPage)
    } catch (error) {
      console.error('Error al eliminar elemento:', error);
      alert('Error al eliminar el elemento: ' + error)
    }
  };

  const handleUpdateBook = async (book: BookModel) => {
    newItem.title = book.title
    newItem.description = book.description
    newItem.id = book.id
    newItem.status = book.status
    setNewItem(newItem)
    setId(book.id!!)
  }

  const updatePage = (value: number) => {
    if (value > 0 && items.length == LIMIT_ELEMENTS_PAGE) {
      fetchData(currentPage + 1)
    } else if (currentPage > 1 && value < 0) {
      fetchData(currentPage - 1)
    }
  }

  const cancelForm = () => {
    setNewItem(buildNewItem());
    setId(-1)
  }

  const handleTextareaChange = (event: any) => {
    const inputValue = event.target.value;
    if (inputValue.length <= 500) {
      setNewItem({ title: newItem.title, description: inputValue, status: newItem.status })
    }
  };

  const handleTitleChange = (event: any) => {
    const inputValue = event.target.value;
    if (inputValue.length <= 200) {
      setNewItem({ title: inputValue, description: newItem.description, status: newItem.status })
    }
  };


  return (
    <div className="contenedor">

      <div className="banner">
        <h1>Libreria</h1>
        <p>Bienvenido!</p>
      </div>

      <div className="div-table">
        <Table
          data={items}
          columnsName={['ID', 'Título', 'Descripción', 'Estado']}
          onDelete={function (id: number) { handleDeleteItem(id); }}
          onUpdate={function (book: BookModel) { handleUpdateBook(book); }}
          changePage={function (value: number) { updatePage(value); }} />

        <hr />

        <div className="formulario">

          <h2>Formulario libro</h2>
          <form>
          <div>
          <small about='sss'>Caracteres restantes: {200 - newItem.title.length}</small>
          <input
              type="text"
              value={newItem.title}
              onChange={handleTitleChange}
              placeholder='Título'
            />
            
          </div>
            
            <select
              id="fruits"
              name="fruits"
              value={newItem.status}
              onChange={(e) => setNewItem({ title: newItem.title, description: newItem.description, status: e.target.value })}
            >
              <option value="" disabled>Seleccione un estado</option>
              <option value="IN_STOCK">IN_STOCK</option>
              <option value="OUT_OF_STOCK">OUT_OF_STOCK</option>
            </select>

            <small>Caracteres restantes: {500 - newItem.description.length}</small>
            <textarea
              value={newItem.description}
              onChange={handleTextareaChange}
              placeholder='Descripción'
              cols={30}
              rows={10}
            />
            

            <label className='boton-cancelar' color='black' onClick={cancelForm}>Cancelar</label>
            <button onClick={handleAddOrUpdateItem}>Guardar</button>
          </form>
        </div>
      </div>

    </div>
  );
};

export default Home;
