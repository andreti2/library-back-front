import React from 'react';
import styled from 'styled-components';
import { BookModel } from '../page';

const TableContainer = styled.table`
  border-collapse: collapse;
  width: 80%;
  margin: auto;
  background-color: #fff;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
`;

const TableHeader = styled.th`
  padding: 15px;
  text-align: left;
  border-bottom: 1px solid #ddd;
  background-color: #4caf50;
  color: #fff;
  font-weight: bold;
`;

const TableData = styled.td`
  padding: 15px;
  text-align: left;
  border-bottom: 1px solid #ddd;
`;

const TableDataPagination = styled.td`
  padding: 15px;
  text-align: center;
  border-bottom: 1px solid #ddd;
`;

interface TableProps {
  data: BookModel[];
  columnsName: string[];
  onDelete: (id: number) => void;
  onUpdate: (book: BookModel) => void;
  changePage: (value: number) => void
}

const Table: React.FC<TableProps> = ({ data, columnsName, onDelete, onUpdate, changePage }) => {

  return (
    <TableContainer>
      <thead>
        <tr>
          {columnsName.map((column, index) => (
            <TableHeader key={index}>{column}</TableHeader>
          ))}
          <TableHeader>Opciones</TableHeader>
        </tr>
      </thead>
      <tbody>
        {data.map((row, rowIndex) => (
          <tr key={rowIndex}>
            <TableData>{row.id}</TableData>
            <TableData>{row.title}</TableData>
            <TableData>{row.description}</TableData>
            <TableData>{row.status}</TableData>
            <TableData>
              <button className='boton-options boton-eliminar' onClick={() => onDelete(row.id!!)}>Eliminar</button>
              <button className='boton-options boton-editar' onClick={() => onUpdate(row)}>Editar</button>
            </TableData>
          </tr>
        ))}
        <tr>
          <TableDataPagination colSpan={4}>
            <button className='boton-pagina' onClick={() => changePage(-1)}>Anterior</button>
            <button className='boton-pagina' onClick={() => changePage(1)}>Siguiente</button>
          </TableDataPagination>
        </tr>
      </tbody>
    </TableContainer>
  );
};

export default Table;
